def add(param1, param2)
   param1 + param2
end

def subtract(param1, param2)
    param1 - param2
end

def sum(arr)
   sum = 0
   arr.each{|element| sum += element}
   sum
end

def multiply(param1, param2 = nil)
    if param2 == nil # we are multiplying all the elements in the provided array
        result = 1
        param1.each{|element| result *= element}
        return result
    end
    param1 * param2
end

def power(a , b)
    result = 1
    b.times {result *= a}
    result
end

def factorial(n)
    result = (n == 0? 1 : n)
    (n-1).downto(2){|multiplier|result *= multiplier}
    result
end
