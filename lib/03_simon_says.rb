Little_words = %w{the and at from to with for over under}

def echo(word)
    word
end

def shout(word)
   word.upcase
end

def repeat(word, times = 2)
    repeated_word = word
   (times-1).times{ repeated_word += " #{word}"}
   return repeated_word
end

def start_of_word(word, length)
    word[0,length]
end

def first_word(sentence)
   sentence.split.first
end

def titleize(title)
    title.capitalize!
    title.split.map do|word| (Little_words.include?(word)|| word == "I")?
    word : word.capitalize
    end.join(" ")
end
