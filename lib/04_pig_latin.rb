Pig_latin = Proc.new do |word|
    beginning = (word.match /\A[^aeiou(qu)]*(qu)?/ )[0]
    "#{word[beginning.length..-1]}#{beginning}ay"
end

def translate(sentence)
   sentence.split.map(&Pig_latin).join " "
end
